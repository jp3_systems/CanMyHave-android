package systems.jp3.canmyhave;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void babyHave(View view) {
        EditText babyText = findViewById(R.id.babyText);
        String message = babyText.getText().toString();
        String urlString="https://www.google.com/search?q=can%20my%20baby%20have%20" + message;
        goSearch(urlString);
    }

    public void dogHave(View view) {
        EditText dogText = findViewById(R.id.dogText);
        String message = dogText.getText().toString();
        String urlString="https://www.google.com/search?q=can%20my%20dog%20have%20" + message;
        goSearch(urlString);
    }

    public void goSearch(String urlString){
        try {
            Uri webpage = Uri.parse(urlString);
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application can handle this request. Please install a web browser.",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


}
